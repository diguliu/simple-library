class CreateExemplars < ActiveRecord::Migration
  def self.up
    create_table :exemplars do |t|
      t.references  :material

      t.timestamps
    end
  end

  def self.down
    drop_table :exemplars
  end
end
