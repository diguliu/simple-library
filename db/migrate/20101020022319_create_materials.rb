class CreateMaterials < ActiveRecord::Migration
  def self.up
    create_table :materials do |t|
      t.string      :type
      t.string      :title, :null => false
      t.integer     :publication_year
      t.string      :authors
      t.string      :tracks
      t.string      :publisher
      t.integer     :edition
      t.integer     :month
      t.integer     :region

      t.timestamps
    end
  end

  def self.down
    drop_table :materials
  end

end
