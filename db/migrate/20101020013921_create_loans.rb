class CreateLoans < ActiveRecord::Migration
  def self.up
    create_table :loans do |t|
      t.datetime    :creation, :null => false
      t.datetime    :forecasted_devolution, :null => false
      t.datetime    :real_devolution
      t.references  :user
      t.references  :material

      t.timestamps
    end
  end

  def self.down
    drop_table :loans
  end
end
