require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "create an user" do
    assert_nothing_raised do
      User.create!(:name => "Sample name", :identifier => "sample-name")
    end
  end

=begin
  test "should not loan the same material simultaneously" do
    user = User.create!(:name => "Sample name", :identifier => "sample-name")
    material = Material.create!(:title => "Sample material")
    assert user.can_perform_loan?(material)

    user.perform_loan(material)
    assert !user.can_perform_loan?(material)
  end
=end
end
