require 'test_helper'

class ExemplarTest < ActiveSupport::TestCase
  test "create an exemplar" do
    assert_nothing_raised do
      Exemplar.create!(:serial_number => 1)
    end
  end
end
