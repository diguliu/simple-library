class User < ActiveRecord::Base
  validates_presence_of :name, :identifier
  validates_uniqueness_of :identifier

  has_many :reservations
  has_many :loans

  ROLES = { :graduation_studend => 1,
            :pos_graduation_student => 2,
            :master => 3,
            :doctor => 4,
            :teacher => 5 }

  def loan_time_limit
    case role
      when ROLES[:graduation_student]
        return 1
      when ROLES[:pos_graduation_student]
        return 2
      when ROLES[:master], ROLES[:doctor]
        return 3
      when ROLES[:teacher]
        return 4
    end
  end

  def loan_quantity_limit
    case role
      when ROLES[:graduation_student]
        return 3
      when ROLES[:pos_graduation_student], ROLES[:master], ROLES[:doctor]
        return 4
      when ROLES[:teacher]
        return 5
    end
  end

  def reservation_quantity_limit
    3
  end

  def pending_loans
    loans.select {|loan| loan.is_late?}
  end

  def active_loans
    loans.select {|loan| loan.is_active?}
  end

  def already_loanning?(material)
    active_loans.any? {|loan| loan.material == material}
  end

  def can_perform_reservation?(material)
    { :material_unavailable => !material.available?(self),
      :pending_loans => !pending_loans.blank?,
      :limit_exceeded => reservations.length >= reservation_quantity_limit }
    
  end

  def perform_reservation(material)
    Reservation.create!(:user => self, :material => material)
  end

  def can_perform_loan?(material)
    { :material_unavailable => !material.available?(self),
      :pending_loans => !pending_loans.blank?,
      :limit_exceeded => active_loans.length >= loan_quantity_limit,
      :already_loanning => already_loanning?(material) }
  end

  def perform_loan(material)
    Loan.create!(:user => self, :material => material, :creation => Time.now, :forecasted_devolution => Time.now+loan_time_limit.days)
    reservation = reservations.find_by_material_id(material.id)
    reservation.destroy if reservation
  end

  def return_material(material)
    loan = loans.opened.find_by_material_id(material.id)
    loan.real_devolution = Time.now
    loan.save!
  end

end
