class Material < ActiveRecord::Base
  validates_presence_of :title

  has_many :reservations
  has_many :exemplars
  has_many :loans

  def available?(user)
    user.reservations.any? { |reservation| reservation.material == self} || 
    loans.opened.length + reservations.length < exemplars.length
  end
  
end
