class Loan < ActiveRecord::Base
  validates_presence_of :creation

  belongs_to :user
  belongs_to :material
  
  scope :opened, :conditions => {:real_devolution => nil} 

  def is_opened?
    !real_devolution
  end

  def is_late?
    is_opened? && Time.now > forecasted_devolution
  end

  def is_active?
    is_opened? && Time.now <= forecasted_devolution
  end

end
